﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF_CHAT
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceChat" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceChat : IServiceChat
    {
        List<ServerUser> users = new List<ServerUser>();
        int nextID = 1;
        public int Connect(string userName)
        {
            ServerUser user = new ServerUser()
            {
                ID = nextID,
                Name = userName,
                OperationContext = OperationContext.Current
            };
            nextID++;
            SendMessage($" {user.Name} Entered the chat");
            users.Add(user);
            return user.ID;
        }

        public void Disconnect(int id)
        {
            var user = users.FirstOrDefault(i => i.ID == id);

            if(user != null)
            {
                users.Remove(user);
                SendMessage($" {user.Name} Left the chat");
            }
        }

        public void SendMessage(string msg, int id = 0)
        {
            foreach(var u in users)
            {
                string answer = DateTime.Now.ToShortTimeString();

                var user = users.FirstOrDefault(i => i.ID == id);

                if(user != null)
                {
                    answer += $": {user.Name} ";
                }
                answer += msg;

                u.OperationContext.GetCallbackChannel<IServerChatCallback>().MessageCalback(answer);
            }
        }
    }
}
